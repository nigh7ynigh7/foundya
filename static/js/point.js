function Point(x, y){
    this.x = x;
    this.y = y;
    
    this.setY = function setY(y){
        this.y = y;
    }
    this.getY = function getY(){
        return this.y;
    }

    this.setX = function setX(x){
        this.x = x;
    }
    this.getX = function getX(){
        return this.x;
    }

    this.setPoint = function setPoint(x, y){
        if(isObject(x)){
            y = x.y;
            x = x.x;
        }
        this.x = x;
        this.y = y;        
    }
    
    this.getPoint = function getPoint(){
        return {
            x : this.x,
            y : this.y
        }
    }

    this.isSet = function isSet(){
        return isNumber(this.x) && isNumber(this.y);
    }
}