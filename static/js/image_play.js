var imageHandler = { image : document.getElementById('playImg') || null };

(function(handler){

    handler.getImage = function getImage(){
        return this.image;
    }

    handler.setImage = function setImage(type, buffer){
        if(type == 'base64'){
            this.image = buffer;
        }
    }

})(imageHandler);