(function(){
    if(!$) throw new Error('Jquery not found');
    if(!squares) throw new Error('Sqaure collection not found');
    if(!responsive) throw new Error('Responsive handler not found');

    $(window).resize(function(){
        squares.collection.forEach(function(sqrEntity){
            
            var element = sqrEntity.getElement();
            var sqr = sqrEntity.getSquare();

            if(!element || !sqr) return;
            
            var point = sqr.getStartingPoint();

            element.style.top = 
                Math.round(responsive.calculateRelative(point.getY())) + 
                'px';
            element.style.left = 
                Math.round(responsive.calculateRelative(point.getX())) + 
                'px';
            
                
            element.style.width = 
                Math.round(responsive.calculateRelative(sqr.getWidth())) + 
                'px';
            element.style.height = 
                Math.round(responsive.calculateRelative(sqr.getHeight())) + 
                'px';
        });
    });
})();