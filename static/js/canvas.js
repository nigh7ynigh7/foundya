
// provides helper function for the canvas.
var canvasHelpers = { event : {}, elements: { squareEnt : null} };

(function(){
    if(!Point) throw new Error('Point class missing');
    if(!Square) throw new Error('Square class missing');
    if(!SquareCollectionEntity) throw new Error('SquareCollectionEntity class missing');
    /**
     * Given an event object and an offset, this will calculate the position
     * of the cursor relative to the parent on which it hovers.
     */
    canvasHelpers.event.calculatePos = function calculatePos(event, off){
        return new Point(event.pageX - off.left, event.pageY - off.top);
    }
})();


// sets up the canvas and makes it ready to accept squares.

(function(){
    var imgOverlay = document.createElement('div');
    imgOverlay.setAttribute('class','imageOverlay');

    var canvas = document.getElementById('canvas');

    if(!canvas)
        throw new Error('Canvas is missing');

    canvas.appendChild(imgOverlay);
    
    if(imageHandler){
        canvas.appendChild(imageHandler.getImage());
    }
})();

// event handlers

(function(){
    if(!$) throw new Error('JQUERY not present');
    if(!Point) throw new Error('Point not present');
    if(!Square) throw new Error('Square not present');
    if(!squares) throw new Error('List of squares not present');

    // fetch html elements
    var canvas = $('#canvas');
    var btnAddSquare = $('#enableAddBox');
    var txtClue = $('#clue');

    // event booleans
    var allowDraw = false;   // assures can draw or not 
    var drawSquare = false; // assures square is being drawn
    var setSquare = false;

    // keeps track of square and active element
    var element = null;
    var square = null;

    // handles event like a game engine :P

    canvas.mousemove(function(event){
        //calculates position
        var point = canvasHelpers.event.calculatePos(event, canvas.offset());

        //while down and moving = drawing
        //if click = make square
        if(allowDraw && drawSquare){
            console.log('test');
            resizeElement();
            if(!square.getPointA() || !square.getPointA().isSet()){
                square.setPointA(point);
            }else{
                square.setPointB(point);
            }
        }
        
        if(setSquare){
            var entity = new SquareCollectionEntity();
            element.innerHTML = txtClue.val();
            square.setToReal();
            entity.setSquare(square);
            entity.setElement(element);
            entity.setMessage(txtClue.val());
            squares.setEntity(entity);
            resetValues();
        }
    });

    // events that happens on HTML and CSS

    canvas.mousedown(function(){
        drawSquare = true;
    }).mouseup(finishSquare).mouseleave(finishSquare);
    
    btnAddSquare.click(function(event){
        if(!txtClue.val()) return alert('You must give me a clue!');
        allowDraw = true;
        btnAddSquare.prop('disabled', true);
        txtClue.prop('disabled', true);
    });

    function finishSquare(event){
        if(allowDraw && drawSquare){
            setSquare = true;
        }
        else{
            drawSquare = false;
        }
    }

    function createElement(){
        element = document.createElement('div');
        element.className = 'square';
        canvas.append(element);
        return element;
    }

    function createSquare(){
        square = new Square();
        return square;
    }

    function resizeElement(){
        if(!element) createElement();
        if(!square) createSquare();
        
        if(square.getPointA() && square.getPointB()){
            var point = square.getStartingPoint();
            element.style.top = Math.round(point.getY()) + 'px';
            element.style.left = Math.round(point.getX()) + 'px';

            element.style.width = Math.round(square.getWidth()) + 'px';
            element.style.height = Math.round(square.getHeight()) + 'px';
        }
    }

    function resetValues(){
        //buttons and forms
        btnAddSquare.prop('disabled', false);
        txtClue.prop('disabled', false);
        txtClue.val('');

        //references and variables
        square = null;
        element = null;
        setSquare = false; // single square ending
        allowDraw = false; // will no longer allow squares to be drawn
    }
})();