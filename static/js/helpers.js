/**
 * Checks to see if value is null or void
 * @param {*} value Any value whatsoever.
 * @returns a boolean value 
 */
function isVoid(value){
    return typeof value === 'undefined' || value === null;
}

/**
 * Checks to see if value is a number
 * @param {*} value Any value whatsoever.
 * @returns a boolean value 
 */
function isNumber(value){
    return typeof value === 'number';
}

/**
 * Checks to see if value is an object.
 * @param {*} value Any value whatsoever.
 * @returns a boolean value 
 */
function isObject(value){
    return typeof value === 'object'
}

/**
 * Checks to see if value is a string
 * @param {*} value Any value whatsoever.
 * @returns a boolean value 
 */
function isString(value){
    return typeof value === 'string'
}

/**
 * Checks to see if value is a boolean
 * @param {*} value Any value whatsoever.
 * @returns a boolean value 
 */
function isBoolean(value){
    return typeof value === 'boolean'
}

/**
 * Checks to see if value is a falsy value
 * @param {*} value Any value whatsoever.
 * @returns a boolean value 
 */
function isEmpty(value){
    return !value;
}


/**
 * Checks to see if value is an instance of a class
 * @param {*} value Any value whatsoever.
 * @param {*} ref Any class.
 * @returns a boolean value 
 */
function isInstance(value, ref){
    return value instanceof ref;
}

/**
 * Expectes 2 numbers to calculate the real:relative ratio. 
 * @param {Number} real the real value of the given object
 * @param {Number} relative the realtive value of the given object
 * @returns {Number}  real:relative ratio
 */
function calculateRatio(a, b){
    // validates input values
    if(!isNumber(a) || !isNumber(b))
        throw TypeError('Real/Relative Not a number');

    var relativeRatio = a / b;
    return relativeRatio;
}