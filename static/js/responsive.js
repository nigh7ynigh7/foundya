var responsive = {
    ratio : 0,
    getRatio : null,
    calculateReal : null,
    calculateRelative : null
};

(function(){
    if(!$) throw new Error('Jquery Missing');
    if(!imageHandler) throw new Error('Missing image handler');
    var canvas = $('#canvas');
    if(!canvas) throw new Error('Canvas missing'); 
    
    responsive.getRatio = function getRatio(){
        var real = imageHandler.getImage().naturalWidth;

        if(!isNumber(real)){
            alert('Browser with HTML5 support required.');
            throw new Error('HTML5 required');
        }
        
        var relative = canvas.width();
        return real / relative;
    }

    responsive.calculateReal = function calculateReal(input){
        if(!isNumber(input)) throw new TypeError('NaN');
        return  input * responsive.getRatio();
    }
    
    responsive.calculateRelative = function calculateRelative(input){
        if(!isNumber(input)) throw new TypeError('NaN');
        return  input / responsive.getRatio();
    }
})();