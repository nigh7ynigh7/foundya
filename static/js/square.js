function Square(x, y, width, height){
    var pointA = new Point();
    var pointB = new Point();
    
    if(x instanceof Point && y instanceof Point){
        pointA.setPoint(x);
        pointB.setPoint(y);
    }
    else if(isNumber(x) && isNumber(y) 
            && isNumber(width) && isNumber(height)){
        pointA.setPoint(x, y);
        pointB.setPoint(x + width, y + height);
    }

    this.setPointA = function(point){
        pointA.setPoint(point);
    }
    this.getPointA = function(){
        return pointA;
    }

    this.setPointB = function(point){
        pointB.setPoint(point);
    }
    this.getPointB = function(){
        return pointB;
    }

    this.getWidth = function getWidth(){
        return Math.abs(pointA.x - pointB.x);
    }

    this.getHeight = function getWidth(){
        return Math.abs(pointA.y - pointB.y);
    }

    this.getStartingPoint = function getStartingPoint(){
        var p = new Point();
        p.setX(pointA.x < pointB.x ? pointA.x : pointB.x);
        p.setY(pointA.y < pointB.y ? pointA.y : pointB.y);
        return p;
    }

    this.getEndingPoint = function getEndingPoint(){
        var p = new Point();
        p.setX(pointA.x > pointB.x ? pointA.x : pointB.x);
        p.setY(pointA.y > pointB.y ? pointA.y : pointB.y);
        return p;
    }

    this.getArea = function getArea(){
        return this.getWidth() * this.getHeight();
    }

    this.setToReal = function setToReal(point){
        if(!responsive) throw new Error('Responsive method missing');
        
        if(!point || point == 'a'){
            pointA.setX(responsive.calculateReal(pointA.getX()));
            pointA.setY(responsive.calculateReal(pointA.getY()));
        }
        if(!point || point == 'b'){
            pointB.setX(responsive.calculateReal(pointB.getX()));
            pointB.setY(responsive.calculateReal(pointB.getY()));
        }
    }

    this.isWithin = function isWithin(point){
        if(!point || !isInstance(point, Point)) throw new TypeError('Input must be a point');

        var start = this.getStartingPoint();
        var end = this.getEndingPoint();

        return point.getX() >= start.getX() && point.getX() <= end.getX() &&
            point.getY() >= start.getY() && point.getY() <= end.getY();
    }
}