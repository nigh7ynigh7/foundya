var squares = {
    collection : []
};

function SquareCollectionEntity(id, square){
    this.id = id;
    this.square = square;
    this.element = null;
    this.message = '';
    
    this.setElement = function setElement(e){
        this.element = e;
    }
    this.getElement = function getElement(){
        return this.element;
    }
    this.getSquare = function getSquare(){
        return this.square;
    }
    this.setSquare = function setSquare(square){
        if(!isInstance(square, Square)) throw new TypeError('Not a Square');
        this.square = square;
    }
    this.getId = function getId(){
        return this.id;
    }
    this.setId = function setId(id){
        if(!isNumber(id)) throw new TypeError('Not a Number');
        this.id = id;
    }
    this.getMessage = function getMessage(){
        return this.message;
    }
    this.setMessage = function setMessage(message){
        this.message = message;
    }
}

(function squaresManagement(){

    var id = 0;
    squares.addSquare = function addSquare(square){
        if(!isInstance(square, Square))
            throw new TypeError('Input must be a square');
        squares.collection.push(new SquareCollectionEntity(++id, square));
        return square;
    }

    squares.getEntity = function getEntity(identifier){
        
        var found = null;
        this.collection.forEach(function searchForEntities(output){
            if(output.id === identifier || output.square === identifier || output === identifier)
                found = output;
        });

        return found;
    }

    squares.setEntity = function setEntity(entity){
        if(!entity instanceof SquareCollectionEntity) return;
        var foundId = entity.getId();
        var found = null;
        if(!isVoid(foundId)){
            found = squares.getEntity(foundId);
        }
        if(found){
            found.setSquare(entity.getSquare());
            found.setElement(entity.getElement());
            return found;
        }
        entity.setId(++id);
        squares.collection.push(entity);
        return entity;
    }

    squares.removeSquare = function removeSquare(squareOrId){
        var operation = null;
        
        if(isNumber(squareOrId))
            operation = 'id';
        else if(isInstance(squareOrId, Square))
            operation = 'square';
        
        if(!operation)
            throw new TypeError('Invalid input parameter');
        
        var found = -1;
        squares.collection.forEach(function rmSqrFinder(squareEntity, index){
            if(operation === 'id'){
                if(squareEntity.id === squareOrId)
                    found = index;
            }else if(operation === 'square'){
                if(squareEntity.square === squareOrId)
                    found = index;
            }
        });

        if(found < 0) return;

        squares.collection.splice(0, 1);
    }

    squares.findById = function findById(id){
        var square = null;

        squares.collection.forEach(function idSqrFinder(squareEntity){
            if(id == squareEntity.id)
                square = squareEntity.square;
        });

        return square;
    }

    squares.toList = function toList(){
        var list = [];
        squares.collection.forEach(function toLstSquareCycler(squareEntity){
            list.push(squareEntity.sqaure);
        });

        return list;
    }

    squares.setList = function setList(list){
        if(!isInstance(list, Array)) throw TypeError('Invalid input');

        list.forEach(function squareSetter(square){
            squares.addSquare(square);
        });
    }
})();

//TODO test