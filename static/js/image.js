//handles image
var imageHandler = { image : new Image() };

(function(){
    imageHandler.image.setAttribute('class', 'backgroundImage');
    
    imageHandler.getImage = function getImage(){
        return this.image;
    }
    imageHandler.setImage = function setImageByBuffer(type, buffer){
        if(type == 'base64')
            this.image.src =  buffer;
    }
    imageHandler.setClass = function setClass(imgClass){
        this.image.className +=  ' ' +  imgClass;
    }
    imageHandler.resetClass = function resetClass(){
        this.image.className = 'backgroundImage';
    }
    imageHandler.setClassIfNotExist = function setClassIfNotExist(imgClass){
        if(this.image.className.includes(imgClass))
            this.image.className += ' ' + imgClass;
    } 
})();

//handles image form

(function(){
    if(!$)
        throw new Error('JQUERY is required for this');
    
    var fileField = $('#file');
    var btnFileUpload = $('#btnFileUpload');
    var fileInfo = $('#imageInformation');

    fileField.on('change', function(event){

        // Loads file from disk
        var fileList = event.target.files;

        if(fileList.length == 0){
            // TODO use bootstrap's thing
            alert('No files were selected. Please select a file.');
            clearFileField();
            return false;
        }
        else if(!validateFile(fileList[0])){
            alert(typeFromMime(fileList[0].type) + ' file type not accepted.');
            loadFileInfo();
            return false;
        }

        // reads as base 64;
        var file = fileList[0];

        loadFileInfo(file.name);

        var reader = new FileReader();
        reader.onloadend = function(){
            var b64 = reader.result;
            
            $('.creator_view').show();
            
            loadImageBase64(b64);
        };
        reader.readAsDataURL(file);
        
        return false;
    });

    btnFileUpload.click(function(e){
        e.preventDefault();
        fileField.click()
        return false;
    });


    function validateFile(file){
        if(!(file instanceof File))
            return false;
        var acceptedTypes = ['jpeg', 'png', 'jpg', 'bmp'];

        var accept = false;
        
        acceptedTypes.forEach(function(type){
            if(file.type.toLowerCase().includes(type)){
                accept = true;
            }
        });
        return accept;
    }

    function loadImageBase64(buffer){
        imageHandler.setImage('base64', buffer);
        imageHandler.setClassIfNotExist('backgroundImage');
    }

    function typeFromMime(type){
        var split = type.split('/');
        return type = split[split.length-1] || type;
    }

    function loadFileInfo(str){
        console.log(str)
        str = str ? str : 'Choose a '
            + '<span class="code-text">png</span>, '
            + '<span class="code-text">jpeg/jpg</span>, or a '
            + '<span class="code-text">bmp</span> image';
        fileInfo.html(str);
    }

    function clearFileField(){
        fileField.val('');
        loadFileInfo();    
    }
    
    loadFileInfo();

})();

