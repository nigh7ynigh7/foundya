var manipulateForm = null;

// deals with form manipulation.
// appends squares after dealing with them.
(function(){
    if(!$) throw new Error('Jquery is missing');
    
    var form = $('#sqform');
    
    if(!form) throw new Error('Form is missing');

    manipulateForm = { };

    var container = null;
    makeContainer();


    manipulateForm.appendSquare = function appendSquare(entity){
        var square = entity.getSquare();
        var id = entity.getId();
        var message = entity.getMessage();
        
        if(square.getArea() <= 1) return alert('Skipping square with hint of "'+
            message+'": Area is too small.');

        container.appendChild(makeInput(id+'x',
            Math.round(square.getStartingPoint().getX())));
        container.appendChild(makeInput(id+'y',
            Math.round(square.getStartingPoint().getY())));
        container.appendChild(makeInput(id+'width',
            Math.round(square.getWidth())));
        container.appendChild(makeInput(id+'height',
            Math.round(square.getHeight())));
        container.appendChild(makeInput(id+'hint', message));
    }

    manipulateForm.getContainer = function getContainer(){
        return container;
    }
    manipulateForm.resetForm = function resetForm(){
        makeContainer();
    }

    function makeInput(name, value){
        var input = document.createElement('input');
        input.setAttribute('name', name);
        input.setAttribute('value', value);
        return input;
    }

    function makeContainer(){
        if(container){
            $(container).remove();
            container = null;
        }
        container = document.createElement('div');
        container.setAttribute('hidden', true);
        form.append(container);
    }

})();

(function(){
    if(!$) throw new Error('Jquery is missing');
    if(!squares) throw new Error('SquareEntityList is missing');
    if(!manipulateForm) throw new Error('ManipulateForm is missing');

    var btnUpload = $('#upload');
    var form = $('#sqform');
    var name = $('#nameOfGame');

    if(!btnUpload || !name || !form) throw new Error('Some elements are missing.');

    btnUpload.click(function(){
        if(!name.val().trim()){
            alert('Name your game, seriously!');
            return false;
        }
        
        manipulateForm.resetForm();

        squares.collection.forEach(function(entity){
            manipulateForm.appendSquare(entity);
        });

        return form.submit();
    });
})();