
var squares = [];

function SquareGroup(){
    this.square = null;
    this.element = null;
    this.id = null;

    this.setSquare = function setSquare(square){
        this.square = square;
    }

    this.getSquare = function getSquare(){
        return this.square;
    }

    this.setElement = function setElement(element){
        this.element = element;
    }

    this.getElement = function getElement(){
        return this.element;
    }
    
    this.setId = function setId(id){
        this.id = id;
    }

    this.getId = function getId(){
        return this.id;
    }
}

// pass through squares and assign hint_elements to sqaures
(function(){

    // checks references to see if we're good to go
    if(!$) throw new ReferenceError('jQuery is required for this.');
    if(!Square) throw new ReferenceError('Square is required for this.');
    if(isVoid(squaresFromDB)) throw new ReferenceError('Squares from database is missing');
    
    // loads squares into collection
    var squareCollection = squaresFromDB;

    // groups the squares up
    squareCollection.forEach(function applyEm(square){

        // checks if references are good to go.
        if(!square) return;

        var id = square.id;
        
        if(!id) return;

        // force instance of square
        var sqr = new Square();
        sqr.setPointA(new Point(square.x, square.y));
        sqr.setPointB(new Point(square.x + square.width, square.y + square.height));

        var element = document.getElementById('hint_'+id);

        if(!element) return;

        // makes a square group
        group = new SquareGroup();

        group.setElement(element);
        group.setId(id);
        group.setSquare(sqr);

        squares.push(group);
    });

})();

// assign click event the image and enable a loop to see if i hit the right spot

(function(){

    // checks references to see if we're good to go
    if(!$) throw new ReferenceError('jQuery is required for this.');
    if(!Point) throw new ReferenceError('Point is required for this.');
    var image = $('#playImg');
    if(!image) throw new ReferenceError('Image of class `playImg` is required for this.');

    var modal = $('#myModal');
    var showModal = true;
    
    // click event for the image
    image.click(function(event){

        // grabs where user clicked
        var point = calculatePos(event, image.offset());

        // if nothing was returned....
        if(!point) throw new Error('Invalid type');

        // convert current relative point to real image points
        var realPoint = new Point(responsive.calculateReal(point.getX()), 
            responsive.calculateReal(point.getY()));

        // search for points that are within the current dude
        var indexFound = [];
        var squaresFound = [];
        squares.forEach(function checkIfHit(group, index){

            if(group.getSquare().isWithin(realPoint)){
                indexFound.push(index);
                squaresFound.push(group);
            }

        });

        // pops the square groups from the array
        indexFound.reverse();
        indexFound.forEach(function remove(index){
            squares.splice(index, 1);
        });
        
        // sets the label as crossed
        squaresFound.forEach(function cross(group){

            if(!group) return;

            var element = group.getElement();
            
            element.className = "crossed";

        });

        // show message after clearing the game
        if(squares.length == 0 && modal && showModal){
            showModal = false;
            modal.modal('show');
        }

    });

    function calculatePos(event, off){
        return new Point(event.pageX - off.left, event.pageY - off.top);
    }

})();