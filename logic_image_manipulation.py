from helpers import extract_ext
from PIL import Image, ImageFilter

def generate_icon(img = None, height_max = 75, width_max = 140):
    if not img:
        raise ValueError('Image cannot be null')

    width, height = img.size;

    # assures that the icon has a max height of 75
    width = int((height_max / height) * width);

    # assures maximum width if bigger than expected
    if width_max < width:
        ratio = width_max / width
        height_max = int(ratio * height_max)
        width = int(ratio * width)

    resized = img.resize((width, height_max), Image.ANTIALIAS);
    resized.seek(0)

    img.seek(0);

    return resized;

def load_image(filename):
    if not filename or len(filename) == 0:
        raise ValueError('File name cannot be empty')

    img_file = open('./uploads/{}'.format(filename), 'b+r')

    img = Image.open(img_file);

    img.seek(0)
    
    return img;

def blur_image(image = None, radius=5):
    if not image:
        raise ValueError('Image cannot be null')
    
    blurred_img = image.filter(ImageFilter.GaussianBlur(radius));
    blurred_img.seek(0);
    image.seek(0);

    return blurred_img;

def close_image(file = None):
    if not file:
        return;

    file.close();