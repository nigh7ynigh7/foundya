from models import Vote, User, Game, Session
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import exc
from sqlalchemy.sql import func
from sqlalchemy.orm import joinedload_all
import re

from helpers import InvalidFieldError, NotFoundError

# save or update 
def vote_game(game_id = 0, user_id = 0, weight=0):
    '''makes a vote'''
    if not game_id or not user_id:
        raise InvalidFieldError('Game and User is required to vote.')
    
    # validate weight
    if weight not in [1, 0, -1]:
        raise InvalidFieldError('Weight must either be 1, 0, or -1. Anything else sucks.');
     
    session = Session();
    
    error = None;
    try:
        game = session.query(Game).filter_by(id=game_id).first();

        if not game:
            raise NotFoundError('Game not found');

        vote = find_by_game_and_user(user_id, game_id, session);

        # update or create vote
        if not vote:
            vote = Vote(game_id=game_id, user_id=user_id, weight=weight);

            if weight == 1:
                game.votes_up = game.votes_up + 1;
            elif weight == -1:
                game.votes_down = game.votes_down - 1;
            
        else:
            
            prev_weight = vote.weight;

            if prev_weight == 1:
                game.votes_up = game.votes_up - 1 if weight == -1 or weight == 0 else game.votes_up;
                game.votes_down = game.votes_down + 1 if weight == -1 else game.votes_down;
            elif prev_weight == -1:
                game.votes_up = game.votes_up + 1 if weight == 1 else game.votes_up;
                game.votes_down = game.votes_down - 1 if weight == 1 or weight == 0 else game.votes_down;
            else:
                game.votes_up = game.votes_up + 1 if weight == 1 else game.votes_up; 
                game.votes_down = game.votes_down + 1 if weight == -1 else game.votes_down; 

            vote.weight = weight;

        # add vote
        session.add(game);
        session.add(vote);
        session.commit();
        session.close();

    except Exception as e:
        
        # if an error has happened:
        session.close();

        raise e;

def find_by_game_and_user(user_id = 0, game_id = 0, session = None):
    '''find a vote by a given user'''
    if not user_id or not game_id:
        raise InvalidFieldError('User and game are missing.');
    
    insession = session;

    if not insession:
        insession = Session();

    try:
        vote = insession.query(Vote).options(joinedload_all('*')).filter_by(user_id=user_id, game_id=game_id).first();
        

        if insession != session:
            insession.expunge(vote);
            insession.close();
        
        return vote;

    except Exception as e:
        
        if insession != session:
            insession.close();
        
        raise e;
