# INTRODUCTION

This is an I Spy, Find Waldo/Wally type of game.

Built using python, mysql, and your normal webstuff.

# Purpose

To be submited as the final project for cs50.

# Prepping the groundwork

Intall all required dependencies.

Simply run

    pip3 install --r requirements.txt

## if using apt

    sudo apt install python3-mysqldb

## create the database & add this script:

If using the bash program

    mysql -uroot -p;

then

    create schema foundya;

# Run the project

Then execute

    ./run

Which comes in the project.

## if you want the _hottest games_ list to work:

execute the following command **after running** the project

    mysql -uroot -p < initial.sql

otherwise, it'll just give you a happy little error