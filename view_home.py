from flask import Blueprint, request, render_template, url_for, session, redirect, flash

from helpers import NotFoundError, InvalidFieldError, login_required

import logic_game as lg;
import logic_users as lu;

home = Blueprint('home', __name__)


@home.route('/')
def index():
    return render_template('home/index.html');

@home.route('/new')
def newest():
    try:
        games = lg.get_newest_games();
        return render_template('game/list_games.html', games=games, newest=True)
    
    except NotFoundError as e:
        return render_template('error/no_games.html', status=404, message=str(e), is_main=True), 404;
    
    except Exception as e:
        return render_template('error/error.html', status=500, message=str(e)), 500;

@home.route('/hot')
def hottest():

    try:
        games, rankings = lg.get_hottest_games();
        return render_template('game/list_games.html', games=games, hottest=True, rankings=rankings)

    except NotFoundError as e:
        return render_template('error/no_games.html', status=404, message=str(e), is_main=True), 404;

    except Exception as e:
        return render_template('error/error.html', status=500, message=str(e)), 500;

@home.route('/u/<username>')
def by_users(username=None):
    try:
        user = None;

        if username:
            user = lu.get_by_name(username)
        else:
            raise InvalidFieldError('User username is missing')
        
        games = lg.get_games_by_user_id(user.id);

        return render_template('game/list_games.html', games=games, user=user);

    except InvalidFieldError as e:
        return render_template('error/error.html', status=400, message=str(e)), 400;

    except NotFoundError as e:
        return render_template('error/no_games.html', status=404, message=str(e)), 404;
    
    except Exception as e:
        return render_template('error/error.html', status=500, message=str(e)), 500;