from models import User, Game, Square, Image, Session
from helpers import NotFoundError

from sqlalchemy.orm import joinedload_all

import re

def get_image_by_id(id = 0):

    if not id:
        raise ValueError('Id is invalid or missing.');
    
    session = Session()

    try:

        image = session.query(Image).options(joinedload_all('*')).filter_by(id=id).first();

        if not image:
            raise NotFoundError('This image does not exist. Sorry.');
        
        session.expunge(image);
        session.close();
    
        return image;
    
    except Exception as e:

        session.close();

        raise e;

def get_image_by_hash(code = ''):

    if not code:
        raise ValueError('Hash code is invalid')

    session = Session()

    try:
        image = session.query(Image).options(joinedload_all('*')).filter_by(hashed=code).first();

        if not image:
            raise NotFoundError('This image does not exist. Sorry.');

        session.expunge(image);
        session.close();

        return image;
    
    except Exception as e:

        session.close();

        raise e;