from flask import request, redirect, url_for, session, flash
from sqlalchemy.orm import class_mapper
from functools import wraps
from PIL import Image
import re;
import os;

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('user_id') is None:
            flash('You must be logged.')
            return redirect(url_for('users.login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

def extract_ext(fileName):
    search = re.search(r'\.(.*)$', fileName)
    
    if not search:
        return None;

    return search.groups()[0];

def extract_ext_mime(mime):
    ls = mime.split('/');

    if not ls or len(ls) == 0:
        return None

    elif len(ls) == 1:
        return ls[1]

    return ls[len(ls) - 1]

def make_directory(dir):
    if not os.path.exists(dir):
        try:
            os.makedirs(dir)
            return True
        except OSError as exc: # Guard against race condition
            return False
    return True

def calculate_dim(flask_file):
    img = Image.open(flask_file);
    return img.size;

def load_or_close_file(reference = None):
    if not reference:
        return;

    if type(reference) == str:
        return open(reference, 'r+b')
    
    close(reference)
    return reference;

class NotFoundError(Exception):
    pass

class InvalidFieldError(Exception):
    pass

class InvalidSquareError(Exception):
    pass

class InvalidGameError(Exception):
    pass