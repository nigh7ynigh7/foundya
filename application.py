# import configuration file
import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# ready and run the app
from flask import Flask, request, render_template

from view_users import usr
from view_home import home
from view_game import game

import models

# initialize app
app = Flask(__name__)

# basic config
if os.environ.get('APP_MAX_REQUEST_SIZE_BYTES'):
    app.config['MAX_CONTENT_LENGTH'] = int(os.environ.get('APP_MAX_REQUEST_SIZE_BYTES'));
else:
    app.config['MAX_CONTENT_LENGTH'] = int(0.75 * 1024**2);
app.secret_key = os.environ.get('APP_KEY') if os.environ.get('APP_KEY') else 'default generic key'

# setup rotes
app.register_blueprint(usr, url_prefix='/users')
app.register_blueprint(game, url_prefix='/game')
app.register_blueprint(home)


@app.errorhandler(404)
def handles_error(e):
    return render_template('error/error.html', status=404, message=str(e)), 404

@app.errorhandler(413)
def handles_error(e):
    return render_template('error/error.html', status=404, message=str(e)), 413

@app.errorhandler(Exception)
def handles_error(e):
    return render_template('error/error.html', status=500, message=str(e)), 500

# run it
if __name__ == '__main__':
    app.run()
    models.base.create_all()

# close it
import atexit
@atexit.register
def closeConnections():
    models.engine.dispose()