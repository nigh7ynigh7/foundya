from sqlalchemy.ext.serializer import loads, dumps
from sqlalchemy.orm import joinedload_all
from sqlalchemy import text

from models import User, Game, Square, Image, Session

from helpers import InvalidFieldError, NotFoundError

def save_game(userid, game, squares, image):
    if not userid or not game or not squares or not image:
        raise ValueError('Not in the games');
    
    height, width = image.image_height, image.image_width

    session = Session();

    error = None

    try:
        game.user_id = userid
        image.user_id = userid
        game.image = image

        for sqr in squares:
            sqr.user_id = userid


            if not sqr.x or not sqr.y or \
                    not sqr.width or not sqr.height or \
                    not sqr.hint:
                    
                raise ValueError('Invalid square.');

            if sqr.x < 0 or sqr.y < 0 or sqr.height <= 0 or sqr.width <= 0: 

                raise ValueError('Invalid square.');
            
            if sqr.width * sqr.height > height * width or \
                    sqr.x + sqr.width > width or \
                    sqr.y + sqr.height > height:

                raise ValueError('Square is out of bounds.')
            sqr.game = game;
        
        game.squares = squares
        session.add(image);
        session.add(game);

        session.commit();

        session.refresh(game);
        session.expunge(game);

        session.close();

        return game;
    except Exception as e:
        
        session.rollback();
        session.close();        
        raise e;

        
def get_game_by_id(id=0):
    if not id:
        raise ValueError('Informed ID is invalid.');
    
    session = Session()

    try:

        game = session.query(Game).options(joinedload_all('*')).filter_by(id=id).first();


        if not game:
            raise NotFoundError('Game not found.');
        
        session.expunge(game);
        session.close();
        
        return game;

    except Exception as e:

        session.close();
        
        raise e

def get_squares_by_game_id(id = 0):
    if not id:
        raise ValueError('Informed ID is invalid.')
    
    session = Session();

    try:

        squares = list(session.query(Square).options(joinedload_all('*')).filter_by(game_id = id));

        if not squares or len(squares) == 0:
            raise Exception('Honestly, I have no idea what happened here.');
        
        for sqr in squares:
            session.expunge(sqr);

        session.close();
        
        return squares;

    except Exception as e:
        
        session.close();

        raise e;

def get_games_by_user_id(id = 0):
    if not id:
        raise ValueError('Informed ID is invalid.')
    
    session = Session()

    try:

        games = list(session.query(Game).options(joinedload_all('*')).filter_by(user_id=id))

        if not games:
            raise NotFoundError('No games here, yet...');

        games = list(games);

        if len(games) == 0:
            raise NotFoundError('No games here, yet...');

        for game in games:
            session.expunge(game);    

        return games;
    
    except Exception as e:

        session.close();

        raise e;
def get_newest_games(limit=40):

    # get most recent games

    session = Session()

    try:

        games = list(session.query(Game).options(joinedload_all('*')).order_by(Game.id.desc()).limit(limit));

        if not games:
            raise NotFoundError('No games here, yet...');

        games = list(games);

        if len(games) == 0:
            raise NotFoundError('No games here, yet...');

        for game in games:
            session.expunge(game);

        return games;

    except Exception as e:

        session.close();

        raise e;

def get_hottest_games(limit = 40):
    
    session = Session()
    
    try:
        results = session.execute('''
            select 
                id,
                calculate_rank(votes_up, votes_down, created_at) as rank
            from games
            where
                time_to_sec(timediff(current_timestamp, created_at)) < 60 * 60 * 24 * 25
            order by rank desc
            limit :limit ''', {'limit' : limit});
        
        # checks if games exists
        if not results:
            raise NotFoundError('No games here, yet...');

        results = list(results)
        
        if len(results) == 0:
            raise NotFoundError('No games here, yet...');
        
        # treats record;
        
        output = []
        rankings = {};

        for record in results:
            output.append(get_game_by_id(record[0]));
            rankings[record[0]] = record[1];
        
        session.close();
        
        return output, rankings;
    except Exception as e:

        session.close();

        raise e;