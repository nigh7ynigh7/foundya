from models import Game, Square, Image
from flask import Blueprint, request, render_template, url_for, session, redirect, flash, send_file
from helpers import NotFoundError, InvalidFieldError, login_required, make_directory, calculate_dim, extract_ext_mime
from logic_game import save_game, get_game_by_id, get_squares_by_game_id

import logic_image_manipulation as lim
import logic_vote as lv
import logic_image
import mimetypes
import uuid
import re
import io
import os

folder = './uploads/'
ext = set(['png', 'jpg', 'jpeg'])


game = Blueprint('game', __name__)


@game.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    if request.method == 'GET':
        return render_template('game/create_update.html');
    user_id = session.get('user_id');

    # handles images

    image_file = request.files.get('file');

    if not image_file:
        flash('You must inform an image');
        return redirect(url_for('game.create'));
    
    image_hash = str(uuid.uuid4());
    mime = image_file.content_type;
    image_name = image_file.filename;

    image_width, image_height = calculate_dim(image_file);

    img = Image(name=image_name, hashed=image_hash, \
        type_of=mime, user_id = user_id, \
        image_height=image_height, image_width=image_width);

    try:
        if make_directory(folder):
            image_file.seek(0);
            image_file.save(folder + image_hash);
        else:
            raise Exception('Could not make folder');
    except Exception as e:
        flash('Failed to save image');
        return redirect(url_for('game.create'));
    
    # handles game entity

    game_name = request.form.get('name');
    if not game_name:
        flash('You must name your game.')
        return redirect(url_for('game.create'));
    
    is_nsfw = request.form.get("is_nsfw");
    print(str(is_nsfw) + str(type(is_nsfw)))
    game = Game(name=game_name, user_id=user_id, is_nsfw=True if is_nsfw else False);

    # handles squares

    squares = {}
    failed = False;
    try:
        for key in list(request.form.keys()):
            result = re.search(r'^(\d+)(\w+)', key)
            
            if not result:
                continue;
            
            result = result.groups();
            index = int(result[0]);
            prop = result[1];
            
            sqr = squares.get(index);
            if not sqr:
                sqr = Square()
                squares[index] = sqr
            if prop == 'hint':
                setattr(sqr, prop, request.form.get(key));
            else:
                setattr(sqr, prop, int(request.form.get(key)));
                
    except Exception as e:
        failed = True;
    
    if failed:
        flash('Failed to parse squares. Aborting.');
        os.remove(folder + image_hash);
        return redirect(url_for('game.create'));

    end_game = None;
    try:
        end_game = save_game(user_id, game, list(squares.values()), img);
    except Exception as e:
        print(str(e));
        flash('Failed to save game.');
        os.remove(folder + image_hash);
        return redirect(url_for('game.create'));                        

    return render_template('game/successful.html', game=end_game);

@game.route('/images/<filehash>', methods=['GET'])
def load_image(filehash):
    if not filehash:
        flash('invalid file name');
        return redirect(url_for('home.index'))
    
    is_icon = request.args.get('icon');
    is_blur = request.args.get('blur');

    try:
        meta = logic_image.get_image_by_hash(filehash);

        img = lim.load_image(meta.hashed)
        icon = False

        if is_icon == '' or is_icon == 'true':
            img = lim.generate_icon(img)
            icon = True

        if is_blur == '' or is_blur == 'true':
            if icon:
                img = lim.blur_image(img);
            else:
                img = lim.blur_image(img, 10);
                

        container = io.BytesIO()
        img.save(container, extract_ext_mime(meta.type_of))
        container.seek(0);

        return send_file(container, mimetype=meta.type_of);

    except NotFoundError as e:

        return send_file('./uploads/NOTEXIST.jpg');

@game.route('/<int:game_id>', methods=['GET'])
def load_game(game_id):
    if not game_id:
        return 'no';
    
    try:
        game = get_game_by_id(game_id);

        squares = get_squares_by_game_id(game.id);

        serialized_sqrs = [ sqr.serialize for sqr in squares];

        image = logic_image.get_image_by_id(game.image_id);

        return render_template('game/play.html', game=game, squares=squares, image=image, serialized_sqrs=serialized_sqrs)

    except ValueError as e:
        return render_template('error/error.html', status=400, message=str(e)), 400;

    except NotFoundError as e:
        return render_template('error/error.html', status=404, message=str(e)), 404;

    except Exception as e:
        return render_template('error/error.html', status=500, message=str(e)), 500;
    

@game.route('/<int:game_id>/vote')
@login_required
def vote(game_id):

    user_id = session.get('user_id');

    weight = request.args.get('weight');

    if weight not in ['1', '0', '-1']:
        flash('You cannot upvote or downvote twice in one game.' );
        return redirect(url_for('game.load_game', game_id=game_id))

    weight = int(weight);

    try:
        lv.vote_game(game_id, user_id, weight);
        
        flash('You have voted for this game')

        return redirect(url_for('game.load_game', game_id=game_id))

    except Exception as e:
        
        return render_template('error/error.html', status=500, message=str(e)), 500;