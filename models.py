import os
from datetime import datetime

from sqlalchemy import create_engine, Column, Integer, String, text, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.types import TIMESTAMP, SmallInteger

base = declarative_base()

class User(base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(65), nullable=False, unique=True)
    hashed = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False, unique=True)
    created_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    updated_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        onupdate=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    
    images = relationship('Image', back_populates='user')
    games = relationship('Game', back_populates='user')
    squares = relationship('Square', back_populates='user')

    
class Image(base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    hashed = Column(String(255), nullable=False)
    type_of = Column(String(50), nullable=False)
    image_height = Column(Integer)
    image_width = Column(Integer)
    created_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    updated_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        onupdate=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)

    user = relationship('User', back_populates='images')

    games = relationship('Game', back_populates='image')

class Game(base):
    __tablename__ = 'games'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    description = Column(String(1024))
    is_nsfw = Column(Boolean(), nullable=False, default=False)

    votes_up = Column(Integer, nullable=False, default=0)
    votes_down = Column(Integer, nullable=False, default=0)
    
    created_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    updated_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        onupdate=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user_name = Column(String(255))
    user = relationship('User', foreign_keys=[user_id], back_populates='games')
    
    image_id = Column(Integer, ForeignKey('images.id'), nullable=False)
    image = relationship('Image', back_populates='games')

    squares = relationship('Square', back_populates='game')



class Square(base):
    __tablename__ = 'squares'
    
    id = Column(Integer, primary_key=True)
    game_id = Column(Integer, ForeignKey('games.id'))
    hint = Column(String(255), nullable=False)
    height = Column(Integer, nullable=False)
    width = Column(Integer, nullable=False)
    x = Column(Integer, nullable=False)
    y = Column(Integer, nullable=False)

    user_id = Column(Integer, ForeignKey('users.id'))

    user = relationship('User', back_populates='squares')

    game = relationship('Game', back_populates='squares')

    @property
    def serialize(self):
        return{
            'id' : self.id,
            'game_id' : self.game_id,
            'user_id' : self.user_id,
            'hint' : self.hint,
            'height' : self.height,
            'width' : self.width,
            'x' : self.x,
            'y' : self.y
        }
    
class Vote(base):
    __tablename__ = 'votes'
    
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    game_id = Column(Integer, ForeignKey('games.id'), primary_key=True)
    weight  = Column(SmallInteger, default=0, nullable=False, server_default='0')

    created_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    updated_at = Column(TIMESTAMP, nullable=False, 
        default=datetime.utcnow, 
        onupdate=datetime.utcnow, 
        server_default=text('CURRENT_TIMESTAMP'))
    
    # user = relationship('User', back_populates='votes')
    # game = relationship('User', back_populates='votes')

    # forces unique allows user to vote once per game
    __table_args__ = (UniqueConstraint('user_id', 'game_id', name='unique_vote_person_game'),)


engine = create_engine('mysql://{}:{}@{}/{}'.format( \
        os.environ.get('DB_USER') if os.environ.get('DB_USER') else 'root', \
        os.environ.get('DB_PASS') if os.environ.get('DB_PASS') else '1234', \
        os.environ.get('DB_HOST') if os.environ.get('DB_HOST') else 'localhost', \
        os.environ.get('DB_SCHEMA') if os.environ.get('DB_SCHEMA') else 'foundya' ), \
    echo=True );
base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)