use foundya;

-- source for ranking function
-- https://medium.com/hacking-and-gonzo/how-reddit-ranking-algorithms-work-ef111e33d0d9

DROP FUNCTION IF EXISTS calculate_rank;

DELIMITER ##

CREATE FUNCTION calculate_rank (votes_up INTEGER, votes_down INTEGER, created_at TIMESTAMP)
    RETURNS DOUBLE
    BEGIN

        -- sets basic variable for the vote
        SET @epoch := TIMESTAMP('1970-01-01 00:00:00');

        -- groups together basic data - delta and created_at date
        SET @delta = votes_up - votes_down;
        SET @created_at = created_at;

        -- calculates the order
        SET @order = LOG10(GREATEST(ABS(@delta), 1));

        -- https://www.techonthenet.com/mysql/loops/if_then.php
        -- fixes the sign

        SET @sign = 0;
        IF @delta < 0
        THEN
            SET @sign = -1;
        ELSEIF @delta > 0
        THEN
            SET @sign = 1;
        END IF;
        
        -- calculates seconds
        SET @seconds = ABS(TIMESTAMPDIFF(SECOND, @epoch, @created_at)) - 1134028003; 
        
        -- calculates final grade for given game_id
        RETURN ROUND( @sign * @order + @seconds / 45000 , 7);

    END ##

DELIMITER ;
