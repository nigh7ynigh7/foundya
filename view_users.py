from flask import Blueprint, request, render_template, url_for, session, redirect, flash

from helpers import NotFoundError, InvalidFieldError
import logic_users as lu
import logic_game as lg
import sys
from sqlalchemy import exc


usr = Blueprint('users', __name__)

@usr.route('/register', methods=['GET','POST'])
def register_user():
    if session.get('user_id'):
        return redirect(url_for('home.index'))

    if request.method == 'GET':
        return render_template('users/register.html')

    if request.method != 'POST':
        return redirect(url_for('home.index', _external=True)), 404
    
    name = request.form.get('name')
    password = request.form.get('password')
    password2 = request.form.get('password2')
    email = request.form.get('email')

    if not name or not password or not password2 or not email:
        flash('All fields are required')
        return render_template('users/register.html', name=name, email=email), 400
    
    if password != password2:
        flash('Passwords do not match')
        return render_template('users/register.html', name=name, email=email), 400
    
    try:
        lu.register_user(name, password, email)
        flash('You are registered. You can now log In.')
        return redirect(url_for('home.index'))
    except InvalidFieldError as e:
        flash('Error: {}'.format(str(e)))
        return render_template('users/register.html', name=name, email=email), 400
    except exc.IntegrityError as e:
        flash('Error: User already exists')
        return render_template('users/register.html', name=name, email=email), 409
    except Exception as e:
        flash('Error: '+str(e))
        return render_template('users/register.html', name=name, email=email), 500

@usr.route('/login', methods=['GET','POST'])
def login():
    if session.get('user_id'):
        flash('You already are logged in!')
        return redirect(url_for('home.index'))
    
    if request.method == 'GET':
        return render_template('users/login.html')
    
    if request.method != 'POST':
        return redirect(url_for('home.index', _external=True)), 404

    name = request.form.get('name')
    password = request.form.get('password')
    
    if not name or not password:
        flash('Both password and username fields are required to log in.')
        return render_template('users/login.html')
    
    try:
        user = lu.login(name, password)
        
        if not user:
            return render_template('users/login.html', name=name), 401
        
        flash('You have been logged in.')

        session['user_id'] = user.id
        session['user_name'] = user.name

        return redirect(url_for('home.index'))

    except InvalidFieldError as e:
        flash('Error: {}'.format(str(e)))
        return render_template('users/login.html', name=name), 400       
    except NotFoundError as e:
        flash(str(e))
        return render_template('users/login.html', name=name), 404   
    except Exception as e:
        flash(str(e))
        return render_template('users/login.html', name=name), 500

@usr.route('/logout')
def logout():
    if session.get('user_id'):
        session.pop('user_id')
    if session.get('user_name'):
        session.pop('user_name')
    if session.get('user_permission'):
        session.pop('user_permission')

    flash('You have been logged out')
    return redirect(url_for('home.index'))
    

@usr.route('/my-games')
def my_games():
    if not session.get('user_id'):
        flash('You have to be logged in');
        return redirect(url_for('home.index'))
    
    try:
        games = lg.get_games_by_user_id(int(session.get('user_id')))

        user = lu.get_by_id(session.get('user_id'));

        return render_template('game/list_games.html', games=games, mines=True, user=user);

    except NotFoundError as e:
        return render_template('error/no_games.html', status=404, message=str(e), is_mines=True), 404;
    
    except Exception as e:
        return render_template('error/error.html', status=500, message=str(e)), 500;