from models import User, Session
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import exc
from sqlalchemy.orm import joinedload_all

import re

from helpers import InvalidFieldError, NotFoundError

def is_email(email=''):
    return re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email)

def valid_username(name=''):
    return re.match(r'^(\w[\w\-\.]+\w|\w+)$', name);

def validates(username, password, email):
    """Assuming username, password and email are filled up. validate each one of them"""

    if not is_email(email):
        return "Invalid Email"
    
    def length(value, min, max):
        return len(value) >= min and len(value) <= max 
    
    if is_email(username):
        return "Username cannot be an email."

    if not length(username, 1, 65):
        return "Username length must be between 1 and 65"
    
    if not valid_username(username):
        return "Your username is not valid. Only alphanumeric, dashes, and dots are allowed."
    
    if not length(password, 1, 255):
        return "Password must be between 1 and 255"
    
    if not length(email, 1, 255):
        return "Email must be between 1 and 255"
    
    return ''

def hash_password(password=''):
    if not password:
        return password
    
    return pwd_context.encrypt(password)

def compare_password(hashed, password):
    if not hashed or not password:
        raise Exception('Hashed password and password must be informed.')
    
    return pwd_context.verify(password, hashed)
    

def register_user(username='', password='', email=''):
    """Registers users using email, password, and username. 2 of which must be unque"""


    if not username or not password or not email:
        raise InvalidFieldError('Fields (Username, password, and email) required')
    
    valid = validates(username, password, email)

    if valid:
        raise InvalidFieldError(valid)
    
    password = hash_password(password)

    error = None

    sess = Session()
    try:
        user = User(name=username, hashed=password, email=email)
        sess.add(user)
        sess.commit()
    except Exception as e:
        sess.rollback()
        error = e
    finally:
        sess.close()
    
    if error:
        raise error;

def login(usermail='', password=''):
    if not usermail or not password:
        raise InvalidFieldError('Inform username or email and password.')

    email = is_email(usermail)

    session = Session()

    try:
        if email:
            user = session.query(User).options(joinedload_all('*')).filter_by(email=usermail).first()
        else:
            user = session.query(User).options(joinedload_all('*')).filter_by(name=usermail).first()
        
        if not user:
            raise NotFoundError('user not found exception.');

        if not compare_password(user.hashed, password):
            raise InvalidFieldError('User password does not match.')
        
        session.expunge(user);

        session.close();

        return user

    except Exception as e:
        session.close();
        raise e
        

def update_user(userid, email='', password=''):
    if not userid:
        raise ValueError('ID must be informed.')

    changeemail, changepass = False, False

    if email:
        changeemail = True
        if not is_email(email):
            raise InvalidFieldError('Invalid email.')
    if password:
        changepass = True
        password = hash_password(password)
    
    sess = Session()

    error = None
    
    try:
        user = sess.query(User).get(userid)
        
        if not user:
            raise NotFoundError('User not found.')

        if changeemail:
            user.email = email
        if changepass:
            user.hashed = password

        sess.commit()

    except Exception as e:
        sess.rollback()
        error = e
    finally:
        sess.close()
    
    if error:
        raise error;

def get_by_name(username):
    if not username:
        raise Exception('Username not informed.')
    
    session = Session()
    
    try:

        user = session.query(User).options(joinedload_all('*')).filter_by(name=username).first()

        if not user:
            raise NotFoundError('User not found.')

        session.expunge(user);
        session.close();

        return user;

    except Exception as e:

        session.close();
        
        raise e

def get_by_id(id):
    if not id:
        raise Exception('Id not informed.')
    
    session = Session();

    try:

        user = session.query(User).options(joinedload_all('*')).filter_by(id=id).first()

        if not user:
            raise NotFoundError('User not found.')
        
        session.expunge(user);
        session.close()

        return user
    except Exception as e:

        session.close();

        raise e